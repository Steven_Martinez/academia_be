'use strict';
module.exports = (sequelize, DataTypes) => {
    const course = sequelize.define('course', {
        external_id: DataTypes.UUID,
        name: DataTypes.STRING(75),
        cod: DataTypes.STRING(10),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    course.associate = function(models) {
        course.hasMany(models.subject, { foreignKey: 'id_course', as: 'subject' });
        course.belongsTo(models.student, { foreignKey: 'id_student', as: 'student' })
        course.hasMany(models.time_schedule, { foreignKey: 'id_course', as: 'time_schedule' })
        course.hasMany(models.assistance, { foreignKey: 'id_course', as: 'assistance' })
        course.hasMany(models.evaluation, { foreignKey: 'id_course', as: 'evaluation' })
    };
    return course;
};