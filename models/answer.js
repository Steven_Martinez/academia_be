'use strict';
module.exports = (sequelize, DataTypes) => {
    const answer = sequelize.define('answer', {
        external_id: DataTypes.UUID,
        type: DataTypes.STRING(50),
        detail: DataTypes.STRING,
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    answer.associate = function(models) {
        answer.belongsTo(models.question, { foreignKey: 'id_question', as: ' question' })
    };
    return answer;
};