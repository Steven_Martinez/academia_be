'use strict';
module.exports = (sequelize, DataTypes) => {
    const content_subject = sequelize.define('content_subject', {
        external_id: DataTypes.UUID,
        route: DataTypes.STRING(150),
        detail: DataTypes.STRING(255),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    content_subject.associate = function(models) {
        content_subject.belongsTo(models.person, { foreignKey: 'id_person', as: 'person' })
        content_subject.belongsTo(models.subject, { foreignKey: 'id_subject', as: 'subject' })
    };
    return content_subject;
};