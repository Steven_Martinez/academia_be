'use strict';
module.exports = (sequelize, DataTypes) => {
    const question = sequelize.define('question', {
        external_id: DataTypes.UUID,
        detail: DataTypes.STRING,
        domain: DataTypes.STRING(50),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    question.associate = function(models) {
        question.belongsTo(models.evaluation, { foreignKey: 'id_evaluation', as: 'evaluation' })
        question.hasMany(models.answer, { foreignKey: 'id_question', as: 'answer' })
    };
    return question;
};