'use strict';
module.exports = (sequelize, DataTypes) => {
    const student = sequelize.define('student', {
        external_id: DataTypes.UUID,
        representative: DataTypes.STRING(100),
        email_representative: DataTypes.STRING(100),
        phone_representative: DataTypes.STRING(10)
    }, { freezeTableName: true });
    student.associate = function(models) {
        //ACOUNT//
        student.belongsTo(models.person, { foreignKey: 'id_person', as: 'person' })
        student.hasMany(models.course, { foreignKey: 'id_student', as: 'course' })
        student.hasMany(models.result_evaluation, { foreignKey: 'id_student', as: 'result_evaluation' })
    };
    return student;
};