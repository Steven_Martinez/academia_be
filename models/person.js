'use strict';
module.exports = (sequelize, DataTypes) => {
    const person = sequelize.define('person', {
        external_id: DataTypes.UUID,
        lastName: DataTypes.STRING(100),
        name: DataTypes.STRING(100),
        phone: DataTypes.STRING(10),
        gender: DataTypes.STRING(1),
        picture: DataTypes.STRING(255)
    }, { freezeTableName: true });
    person.associate = function(models) {
        //ROLE//
        person.belongsTo(models.role, { foreignKey: 'id_role', as: 'role' })
            //PERSON//
        person.hasOne(models.student, { foreignKey: 'id_person', as: 'student' })
        person.hasOne(models.teacher, { foreignKey: 'id_person', as: 'teacher' })
        person.hasOne(models.receptionist, { foreignKey: 'id_person', as: 'receptionist' })
            //ACOUNT//
        person.hasOne(models.account, { foreignKey: 'id_person', as: 'account' })
            //IDENTITY DOCUMENT//
        person.hasOne(models.identity_document, { foreignKey: 'id_person', as: 'identity_document' })
    };
    return person;
};