'use strict';
module.exports = (sequelize, DataTypes) => {
    const identity_document = sequelize.define('identity_document', {
        external_id: DataTypes.UUID,
        nro_document: { type: DataTypes.STRING(15), unique: true },
        type: DataTypes.STRING(10),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    identity_document.associate = function(models) {
        //PERSON//
        identity_document.belongsTo(models.person, { foreignKey: 'id_person', as: 'person' });
    };
    return identity_document;
};