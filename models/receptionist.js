'use strict';
module.exports = (sequelize, DataTypes) => {
    const receptionist = sequelize.define('receptionist', {
        external_id: DataTypes.UUID,
        cod: { type: DataTypes.STRING(15), unique: true },
    }, { freezeTableName: true });
    receptionist.associate = function(models) {
        //PERSON//
        receptionist.belongsTo(models.person, { foreignKey: 'id_person', as: 'person' });
    };
    return receptionist;
};