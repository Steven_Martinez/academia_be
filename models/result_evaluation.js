'use strict';
module.exports = (sequelize, DataTypes) => {
    const result_evaluation = sequelize.define('result_evaluation', {
        external_id: DataTypes.UUID,
        title: DataTypes.STRING(125),
        correct_questions: DataTypes.INTEGER(3),
        incorrect_questions: DataTypes.INTEGER(3),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    result_evaluation.associate = function(models) {
        result_evaluation.belongsTo(models.student, { foreignKey: 'id_student', as: 'student' })
        result_evaluation.belongsTo(models.evaluation, { foreignKey: 'id_evaluation', as: 'evaluation' })
    };
    return result_evaluation;
};