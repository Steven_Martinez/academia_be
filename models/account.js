'use strict';
module.exports = (sequelize, DataTypes) => {
    const account = sequelize.define('account', {
        external_id: DataTypes.UUID,
        email: { type: DataTypes.STRING(100), unique: true },
        password: DataTypes.STRING(128),
        is_active: DataTypes.BOOLEAN,
        is_logged: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    account.associate = function(models) {
        //PERSON//
        account.belongsTo(models.person, { foreignKey: 'id_person' });
    };
    return account;
};