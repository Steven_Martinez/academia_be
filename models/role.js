'use strict';
module.exports = (sequelize, DataTypes) => {
    const role = sequelize.define('role', {
        external_id: DataTypes.UUID,
        name: { type: DataTypes.STRING(50), unique: true },
    }, { freezeTableName: true });
    role.associate = function(models) {
        //PERSON//
        role.hasMany(models.person, { foreignKey: 'id_rol', as: 'person' });
    };
    return role;
};