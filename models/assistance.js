'use strict';
module.exports = (sequelize, DataTypes) => {
    const assistance = sequelize.define('assistance', {
        external_id: DataTypes.UUID,
        date: DataTypes.DATE,
        time: DataTypes.TIME,
        detail: DataTypes.STRING(200),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    assistance.associate = function(models) {
        assistance.belongsTo(models.course, { foreignKey: 'id_course', as: 'course' })
    };
    return assistance;
};