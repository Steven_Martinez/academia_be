'use strict';
module.exports = (sequelize, DataTypes) => {
    const time_schedule = sequelize.define('time_schedule', {
        external_id: DataTypes.UUID,
        date: DataTypes.DATE,
        time_init: DataTypes.TIME,
        time_finish: DataTypes.TIME,
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    time_schedule.associate = function(models) {
        time_schedule.belongsTo(models.course, { foreignKey: 'id_course', as: 'course' })
    };
    return time_schedule;
};