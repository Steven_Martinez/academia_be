'use strict';
module.exports = (sequelize, DataTypes) => {
    const teacher = sequelize.define('teacher', {
        external_id: DataTypes.UUID,
        titulo: DataTypes.STRING(100),
    }, { freezeTableName: true });
    teacher.associate = function(models) {
        //PERSON//
        teacher.belongsTo(models.person, { foreignKey: 'id_person', as: 'person' })
        teacher.hasMany(models.subjects, { foreignKey: 'id_teacher', as: 'subjects' })
    };
    return teacher;
};