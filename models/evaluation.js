'use strict';
module.exports = (sequelize, DataTypes) => {
    const evaluation = sequelize.define('evaluation', {
        external_id: DataTypes.UUID,
        title: DataTypes.STRING(125),
        duration: DataTypes.TIME,
        detail: DataTypes.STRING(255),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    evaluation.associate = function(models) {
        evaluation.belongsTo(models.course, { foreignKey: 'id_course', as: 'course' })
        evaluation.hasMany(models.question, { foreignKey: 'id_evaluation', as: 'question' })
        evaluation.hasMany(models.result_evaluation, { foreignKey: 'id_evaluation', as: 'result_evaluation' })
    };
    return evaluation;
};