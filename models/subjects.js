'use strict';
module.exports = (sequelize, DataTypes) => {
    const subjects = sequelize.define('subjects', {
        external_id: DataTypes.UUID,
        name: DataTypes.STRING(75),
        cod: DataTypes.STRING(10),
        status: DataTypes.BOOLEAN
    }, { freezeTableName: true });
    subjects.associate = function(models) {
        subjects.hasMany(models.content_subject, { foreignKey: 'id_subject', as: 'content_subject' });
        subjects.belongsTo(models.teacher, { foreignKey: 'id_teacher', as: 'teacher' });
        subjects.belongsTo(models.course, { foreignKey: 'id_course', as: 'course' });
    };
    return subjects;
};